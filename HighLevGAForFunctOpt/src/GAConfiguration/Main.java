 
package GAConfiguration;

import gaforfunctoptinherit.*;
import java.util.Scanner;
import java.util.Random;

/**
 *
 * @author Ummar Shehu
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        
        Scanner input = new Scanner( System.in );
        System.out.print( "Please Enter the Array Index (0 -9) for the Function Type: " );
        GAConfigChrom gAConfigChrom = new GAConfigChrom (Functions.HYPER_HEURS_FUNS[input.nextInt()], 
                               new Random(RandomSeeds.SEEDS[0]));
             
                GAOperators newGAOperators;
                GA newGA;
                        
                newGAOperators = 
                 
        //For evolving GA (configuration)                    
        new GAOperators (/*chromosome type*/gAConfigChrom, /*population Size*/30, 
                    /*Random object*/gAConfigChrom.getRandom(),
                    /*selection method type*/2, /*crosover type*/1, /*crossover rate*/0.8, 
                    /*mutation type*/2, /*mutation rate*/0.05, /*tournament size*/2, 
                    /*number of generations*/50);                
                 
         newGA = new GA(newGAOperators);
         
         System.out.println(newGA.getGAOp().getStrGAOp() + "\n");
         
         newGA.run();
                        
         
                        
         
            
         
            
                //System.out.println("Functon Type: " + ((FunctChrom)((GAConfigChrom)newGA.getBstRnChrom())
                        //.getChromType()).getFunctType() + "\n");
                
              System.out.println("Chromosome Length: " +  newGA.getBstRnChrom().getChromLength());
            //if (newGA.getBstRnChrom() instanceof GAConfigChrom){
                System.out.println("Evolved (Best) Chromosome Parameter Configuration: \n" + 
                    ((GAConfigChrom)newGA.getBstRnChrom()).getLLevGAStRep());
                
                System.out.println("Fitness: " + newGA.getBstRnChrom().getFitness());
            //}
            
             
    }
   
}


