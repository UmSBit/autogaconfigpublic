/*
 * This package is for a system (high-level GA) for evolving GA configuration 
 * of another GA(low-level)
 */
package GAConfiguration;

import gaforfunctoptinherit.*;

import java.util.ArrayList;

import java.util.Random;

/**
 *
 * @author Ummar Shehu
 * 
 */


/**
 *
 * @author Ummar Shehu (213573268)
 */
public class GAConfigChrom extends Chromosome {
    
    /*
    Each instance of this class represent a complete Genetic Algorithm. It specifies the 
    parameters values (e.g. population size, crossover rates e.t.c) and genetic operators 
    (e.g. crossover type, mutation type e.t.c) of the GA.
    */
    
    //VALS_OF_COMPS represents the range of possible values each component of the low-level GA can have
    private static final double [][] VALS_OF_COMPS = {/*popSizes{20, 50, 100, 200, 300, 400, 500},*/ 
        /*selection method types*/{0, 1, 2}, /*crossover method types*/{0, 1, 2, 3}, 
        /*crossover rates*/{0.6, 0.7, 0.8}, /*mutation type*/ {0, 1, 2, 3}, 
        /*mutation rates*/{0.05, 0.01, 0.1},
        /*non-uniform mutation rates*/{0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 
            0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9},       
        /*shape parameter*/ {0.05, 0.5, 3, 4,/**/ 5, 7, 10, 13, 15},
        /*tournament sizes*/ {2, 5, 10, 15, 20}, /*number of generations {500, 750, 1000},*/ 
        /*sequence*/ {0, 1}};
    
    private ArrayList <Double> alleles;//array list consisting of the configuration of GA
    
    //alleles (/*index 0: popSize*/, 
                 //*index 1: selection method type*/, 
                 //*index 2: crossover method type*/, 
                 //*index 3: crossover rate*/, 
                 //*index 4: mutation type*/, 
                 //*index 5: mutation rate*/,
                 //*index 6: non-uniform mutation rate*/, 
                 //*index 7: shape parameter*/,
                 //*index 8: tournament size*/, 
                 //*index 9: number of generations*/, 
                 //*index 10: sequence*/)
    
    private final Chromosome chromType;
    private double randAllele;
    
    private final Random newRandom;
    
    private String  lowLevGAStrRep;
    
    /**
     *
     * @param gAConfigChrom
     */
    protected GAConfigChrom(GAConfigChrom gAConfigChrom){
       
       setChromLength(gAConfigChrom.getChromLength());
        
        alleles = (ArrayList)(gAConfigChrom.getAlleles().clone());
        Chromosome tmpChromype = null;
        
        try{ 
            
            tmpChromype = gAConfigChrom.getChromType().clone();
            
         }catch(CloneNotSupportedException c){}
        
        chromType = tmpChromype;
        setFitness(gAConfigChrom.getFitness());
        
        
        randAllele = gAConfigChrom.getRndAllele();
        
        newRandom = gAConfigChrom.getRandom();
        
        lowLevGAStrRep = gAConfigChrom.lowLevGAStrRep;
        
    }
    
    /**
     *
     * @param chromType
     * @param newRandom
     */
    public GAConfigChrom(final Chromosome chromType, Random newRandom){
       
        alleles = new ArrayList();
        this.chromType = chromType;
        this.newRandom = newRandom;
        
    }
    
    @Override
    public Chromosome clone()throws CloneNotSupportedException{ 
       
        return new GAConfigChrom(this);  
        
    }
    
    /**
     * 
     * @param locus
     * @param allele 
     */
    @Override
    public void setAllele(int locus, Object allele) {
        
       alleles.remove(locus);
       alleles.add(locus, (Double) allele);
      
    }
    
    /**
     * 
     * @param locus
     * @return 
     */
    @Override
    public Object getAllele(int locus){
       
       return alleles.get(locus);
       
    }
    
    @Override
    public String toString(){
        
        return String.format(
               "System Type: %s%n%s%nChromosome vaues:%n%s"
                       + "%nChromosome fitness:%n%s%n",
                       "GA Configuration System",((FunctChrom)getChromType()).getStrChrom(), 
                        getAlleles(), getFitness());
        
    }
    
    @Override
    public String getStrChrom(){
        
        return String.format(
               "System Type: %s%n"
                       + "%nHigh-Level GA Details:%n",
                       "GA Configuration System" /*, getChromLength()*/);
        
    }
    
    /**
     *
     * @return
     */
    public Chromosome getChromType(){
       return chromType;
    }
    
   @Override
   public Double getRndAllele(){
       return randAllele;
   } 
   
    /**
     *
     * @return
     */
    public double [][] getVALS_OF_COMPS(){
       return VALS_OF_COMPS;
   } 
   
    /**
     *
     * @return
     */
    public Random getRandom(){
       return newRandom;
   } 
    
    /**
     *
     * @return
     */
    public ArrayList <Double> getAlleles() {
       return alleles;
   }
   
    /**
     *
     * @return
     */
    public String getLLevGAStRep(){
       return lowLevGAStrRep;
   } 
   
   @Override
   public void genRndAlleles (Random newRandom){
       
       for (int i = 0; i < VALS_OF_COMPS.length /*alleles.size()*/; i++){
           
           genRandAllele(i, newRandom, 0, 0);
           
           alleles.add(i, getRndAllele());
       }
       
       setChromLength(alleles.size());
       
    }
   
    /**
     *
     * @param chromLoc
     * @param newRandom
     * @return
     */
   
   public int genRandInd(int chromLoc, Random newRandom) {
       
      return newRandom.nextInt(VALS_OF_COMPS [chromLoc].length);
   }
   
   @Override
   public void genRandAllele(int chromLoc, Random newRandom, double lowerBound, 
            double upperBound) {
       
       int randInd;
       randInd = genRandInd(chromLoc, newRandom);
       
       randAllele = VALS_OF_COMPS [chromLoc][randInd];
      
   }
   
   @Override
   public void evalFitness (){
        
        //VALS_OF_COMPS = {/*popSizes*/{20, 50, 100, 200, 300, 400, 500}, 
        //*selection method types*/{0, 1, 2}, /*crossover method types*/{0, 1, 2, 3}, 
        //*crossover rates*/{0.6, 0.7, 0.8}, /*mutation type*/ {0, 1, 2, 3}, /*mutation rates*/{0.05, 0.01, 0.1},
        //*non-uniform mutation rates*/{0.04, 0.05, 0.06}, /*shape parameter*/ {0.05, 0.5, 3, 4},
        //*tournament sizes*/ {2, 5, 10, 15, 20}, /*number of generations*/ {500, 750, 1000}, 
        //*sequence*/ {0, 1}};  
        
    final double [] fitsOfBsRns = new double [RandomSeeds.SEEDS.length];
    double avgFitOfRuns;// = 0;
    double stdDevOfRuns;// = 0;
    double bstFtForAllRns = 0;
    final Statistics newStat = new Statistics ();
    int noOfDiplay = 1;
       
       GA bestGA = null;
         
            for (int j = 0; j < RandomSeeds.SEEDS.length/*3*/; j++){
                
                GAOperators gAConfigGAOP;
                GA newGA;
                        
                gAConfigGAOP = 
                
            /*GAConfigGAOP (Chromosome chromType, int popSize, Random newRandom, int selnMethType, 
            int crosovType, double crossoverRate, int mutatnType, double mutationRate, 
            double nonUnifMutationRate, double shapeParameter,int tournamentSize, 
            int maxNoOfGens, int sequence)*/        
                        
                new GAConfigGAOP (chromType, /*alleles.get(0).intValue()*/ 500, 
                       new Random(RandomSeeds.SEEDS[j]), alleles.get(0).intValue(), 
                        alleles.get(1).intValue(), 
                       alleles.get(2), 
                       alleles.get(3).intValue(), 
                       alleles.get(4), alleles.get(5), alleles.get(6), 
                       alleles.get(7).intValue(), /*alleles.get(9).intValue()*/ 1000, 
                        alleles.get(8).intValue());
                
                        newGA = new GA(gAConfigGAOP);
                        
                        newGA.run();
                        
                        fitsOfBsRns[j] = newGA.getBestRnFt();
                        
                        if (j == 0){
                    
                            bstFtForAllRns = newGA.getBestRnFt();
                            
                            bestGA = newGA;
                            
                           lowLevGAStrRep = gAConfigGAOP.getStrGAOp(); 
                        }
                        
                        else {
                            if (bstFtForAllRns > newGA.getBestRnFt()){
                   
                                bstFtForAllRns = newGA.getBestRnFt();
                                
                                bestGA = newGA;
                                
                            }
                        }

                        if (noOfDiplay == 1){
                    
                            noOfDiplay++;
                    
                        }
                  
                }
            
                noOfDiplay--;

                avgFitOfRuns = newStat.calcAverage (fitsOfBsRns);
                stdDevOfRuns = newStat.calcStdDev(fitsOfBsRns);
                
                setFitness(avgFitOfRuns);
                
   }
   
} 
