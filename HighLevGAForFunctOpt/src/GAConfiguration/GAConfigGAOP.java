/*
 * This package is for a system (high-level GA) for evolving GA configuration 
 * of another GA(low-level)
 */
package GAConfiguration;

import gaforfunctoptinherit.Chromosome;

import gaforfunctoptinherit.*;
import java.util.ArrayList;

import java.util.Random;


/**
 *
 * @Umar Shehu
 * 
 * Instances of this class will only be use for low-level, because the super class
 * of this class is sufficient for the high-level GA.
 */
public class GAConfigGAOP extends GAOperators {
    
    private final int sequence;
    
    private String strSequence;
    
    /**
     *
     * @param chromType
     * @param popSize
     * @param newRandom
     * @param selnMethType
     * @param crosovType
     * @param crossoverRate
     * @param mutatnType
     * @param mutationRate
     * @param nonUnifMutationRate
     * @param shapeParameter
     * @param tournamentSize
     * @param maxNoOfGens
     * @param sequence
     */
    public GAConfigGAOP (Chromosome chromType, int popSize, Random newRandom, int selnMethType, 
            int crosovType, double crossoverRate, /**/int mutatnType, double mutationRate, 
            double nonUnifMutationRate, double shapeParameter,int tournamentSize, 
            int maxNoOfGens, int sequence) {
        
        super (chromType, popSize, newRandom, selnMethType, 
            crosovType, crossoverRate, mutatnType, mutationRate, 
            nonUnifMutationRate, shapeParameter, tournamentSize, maxNoOfGens);
        
        this.sequence = sequence;
        
        if (sequence == 0){
            strSequence = "crossover then mutation";
        }
        
        if (sequence == 1){
            strSequence = "mutation then crossover";
        }
    }
    
    /**
     *This constructor is use for running (testing) low-level GA
     * @param chromType
     * @param newRandom
     * @param alleles
     */
    public GAConfigGAOP (Chromosome chromType, Random newRandom, ArrayList <Double> alleles) {
        
        //GAOperators (/*chromType*/Functions.HYPER_HEURS_FUNS[k], 
                        //*popSize*/500, /*seed*/RandomSeeds.SEEDS[j], 
                        //*selnMethType*/2, /*crosovType*/1, /*crossoverRate*/0.8, 
                        //*mutatnType*/3, /*mutationRate*/1, /*nonUnifMutationRate*/0.06, 
                        //*shapeParameter*/0.05, /*tournamentSize*/20, /*maxNoOfGens*/1000);
                        
        //alleles (
        //*index 0: popSize*/, 
        //*index 1: selection method type*/, /*index 2: crossover method type*/, 
        //*index 3: crossover rate*/, /*index 4: mutation type*/, /*index 5: mutation rate*/,
        //*index 6: non-uniform mutation rate*/, /*index 7: shape parameter*/,
        //*index 8: tournament size*/, /*index 9: number of generations*/, 
        //*index 10: sequence*/)                
        
        
        super (chromType, 500,  
                       newRandom, alleles.get(0).intValue(), 
                        alleles.get(1).intValue(), 
                       alleles.get(2), 
                       alleles.get(3).intValue(), 
                       alleles.get(4), alleles.get(5), alleles.get(6), 
                       alleles.get(7).intValue(), 1000);
        
        sequence = alleles.get(8).intValue();
        
        if (sequence == 0){
            strSequence = "crossover then mutation";
        }
        
        if (sequence == 1){
            strSequence = "mutation then crossover";
        }
    }
    
    /**
     *
     * @param newGAConfigGAOp
     */
    protected GAConfigGAOP(GAConfigGAOP newGAConfigGAOp){
        
        super (newGAConfigGAOp);
        sequence = newGAConfigGAOp.sequence;
        
    }
    
    @Override
    public GAConfigGAOP clone()throws CloneNotSupportedException{
        return  new GAConfigGAOP(this);
    }
    
    @Override
    public void genNewPop (int noOfGens){
        
        if (sequence == 0){
            
            super.genNewPop(noOfGens);
               
        }
        
        if (sequence == 1){
        
            for (int i = 0; i < getCurrentPop ().getPopSize();) {
           
                selectChroms();
           
                mutateChroms(noOfGens);
               
                crossoverChroms ();
           
           addSelChromToNewPop(getSelChroms()[0], i);
           addSelChromToNewPop(getSelChroms()[1], (i + 1));
           i += 2;
           
           }
       
        
           getNewPop().calcPopFit();
        
            elitism (getCurrentPop (), getNewPop());
            getNewPop().findBestChrom();
            getNewPop().findWorstChrom();
        
         }
        
    }
    
    @Override
    public String getStrGAOp(){
        
        return String.format("%s" + "%nSequence: %s%n", super.getStrGAOp(), strSequence);
        
    }
    
}
