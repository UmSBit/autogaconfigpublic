/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaforfunctoptinherit;

/**
 *
 * @author Ummar Shehu 
 */
public class Functions {
    
    private static final int FUN_LEN = 10;
     
    /**
     *Functions commonly use in hyper-heuristics literature
     */
    public  static final Chromosome [] HYPER_HEURS_FUNS  = 
     { 
        
        new SphereChrom(FUN_LEN, new double[][] {{-100, 100}}),
        new RosenChrom(FUN_LEN, new double[][] {{-30, 30}}),
        
        new QuarticChrom(FUN_LEN, new double[][] {{-1.28,1.28}}),
        
        new SchwefelChrom(FUN_LEN, new double[][] {{-500, 500}}),
        new RastriginChrom(FUN_LEN, new double[][] {{-5.12, 5.12}}),
        new GriewangkChrom(FUN_LEN, new double[][] {{-600, 600}}),
        new AckleyChrom(FUN_LEN, new double[][] {{-32, 32}}),
        
        new AddFunct1Chrom(), 
        new AddFunct2Chrom(), 
        new AddFunct3Chrom()
           
     };
}
