 
package gaforfunctoptinherit;

import java.util.Random;

/**
 *
 * @author Ummar Shehu
 */
public class Population {
    
    private final int popSize;//size of the population
    
    //The index (in the array of population of chromosome) of the chromosome with best fitness
    private int bestFitIndex;
    //The index (in the array of population of chromosome) of the chromosome with worst fitness
    private int worstFitIndex;
    
    //The best chromosome in the population
    private Chromosome bestChrom;
    //The worst chromosome in the population
    private Chromosome worstChrom;
    
    //Array of chromosomes of the population
    private final Chromosome [] popIndividuals;
    
    /**The type of chromosome for the population. This determines or is determined by 
      *the problem domain considered
    */
    private final Chromosome popChromType;
    
    /**
     *
     * @param popChromType
     * @param popSize
     */
    public Population (Chromosome popChromType, int popSize){ 
        this.popSize = popSize;
        this.popChromType = popChromType;
        
        popIndividuals = new Chromosome[popSize];
        
        try{
            
            for (int i = 0; i < popIndividuals.length; i++){
             
                popIndividuals[i] = /*(Chromosome)*/popChromType.clone();
                   
            }
            
        }catch(CloneNotSupportedException c){}
        
    }
    
    /**
     *Copy constructor
     * @param population
     */
    public Population(Population population){
        popSize = population.getPopSize();
        
        popIndividuals = new Chromosome [popSize];
        popChromType = population.getPopChromType();
        try{ 
            
            for (int i = 0; i < popSize; i++){
                popIndividuals[i] = population.getPopIndividuals()[i].clone();
                
            }
            
            bestFitIndex =  population.getBestFtIndex();
            
            worstFitIndex =  population.getWorstFtIndex();
        
        }catch(CloneNotSupportedException c){}
        
    }
    
    /**
     *For setting the best population chromosome
     * @param chrom
     */
    public void setBestChrom(Chromosome chrom){
        
        try{ 
            
            bestChrom = chrom.clone();
            
        }catch(CloneNotSupportedException c){}
        
    }
    
    /**
     *For setting the best population chromosome
     * @param chrom
     */
    public void setWorstChrom(Chromosome chrom){
        
        try{ 
            
            worstChrom = chrom.clone();
            
        }catch(CloneNotSupportedException c){}
        
    }
    
    /**
     *For randomly generatng the initial population
     * @param newRandom
     */
    public void genInitPop (Random newRandom){
        
        for (Chromosome chrom: getPopIndividuals()){
            
            chrom.genRndAlleles(newRandom);
            
        }
        
    } 
    
    /**
     *This method can be use for computing the fitness of both the initial 
     *and subsequent populations
     */
    public void calcPopFit(){
        
        for (int i = 0; i < popSize; i++){
            
            popIndividuals[i].evalFitness();
            
        }
        findBestChrom();
        findWorstChrom();
    }
    
    @Override
    public Population clone()throws CloneNotSupportedException{
        
            return new Population (this);
        
    }
    
    /**
     *For returning the population of chromosomes
     * @return
     */
    public Chromosome [] getPopIndividuals (){
        return popIndividuals;
    }
    
    /**
     *For obtaining the worst fitness and its index in chromosomes population array 
     */
    public void findBestChrom (){
        
        double bestFit = popIndividuals[0].getFitness();
        bestFitIndex = 0;
        
        try{
            bestChrom = (Chromosome) popIndividuals[0].clone();
            
        for (int i = 1; i < popSize; i++){

                  if (popIndividuals[i].getFitness() < bestFit){
                      
                     bestFit = popIndividuals[i].getFitness();
                     bestFitIndex = i;
                     bestChrom = (Chromosome) popIndividuals[i].clone();
                    
                 }  
            
        }
               
        }catch(CloneNotSupportedException c){}
        
    }
    
    /**
     *For obtaining the worst fitness and its index in chromosomes population array
     */
    public void findWorstChrom (){
        
        double worstFit = popIndividuals[0].getFitness();
        worstFitIndex = 0;
        
        try{
            
        worstChrom = (Chromosome) popIndividuals[0].clone();
        
        for (int i = 1; i < popSize; i++){

                 if (worstFit < popIndividuals[i].getFitness()){
                     
                     worstFit = popIndividuals[i].getFitness();
                     worstFitIndex = i;
                     worstChrom = (Chromosome) popIndividuals[i].clone();
                     
                 }  
        }
            
        }catch(CloneNotSupportedException c){}
        
    }
    
    /**
     *
     * @return
     */
    public Chromosome getBestChrom(){
        return bestChrom;
    }
    
    /**
     *
     * @return
     */
    public int getBestFtIndex(){
        return bestFitIndex;
    }
    
    /**
     *
     * @return
     */
    public Chromosome getWorstChrom(){
        return worstChrom;
    }
    
    /**
     *
     * @return
     */
    public int getWorstFtIndex(){
        return worstFitIndex;
    }
    
    /**
     *
     * @return
     */
    public int getPopSize(){
        return popSize;
    }
    
    /**
    *For obtaining the string representation of each chromosome of the population
    */
    private String getPopChromVals(){
        
        String returnStr = "\n";
        
        for (int i = 0; i < getPopSize(); i++){
            
            returnStr += "\n" + "(" + i + "):" + getPopIndividuals()[i].toString();
            
        }
        
        return returnStr;
        
    }
    
    /**
     *For returning the string representation of the population without 
      the alleles of ech chromosome of he population
     * @return
     */
    public String getStrPop(){
        
        String returnStr = String.format("%s" +
               "Population Size: %s%n",
                      popIndividuals[0].getStrChrom(), 
                       getPopSize());
        
        return returnStr;
        
    }
    
    /**
     * For obtaining the string representation of the population
     */ 
    @Override
    public String toString(){
        
        String returnStr = String.format(/*"%s" +*/
               "Population Size: %s%nPopulation Individuals: %s",
                getPopSize(), getPopChromVals());
        
        return returnStr;
    }
    
    /**
     *
     * @return
     */
    public Chromosome getPopChromType (){
        return popChromType;
    }
    
}
