
package gaforfunctoptinherit;

/**
 *
 * @author Ummar Shehu 
 */
public class SchwefelChrom extends /*SameBndChrom*/ FunctChrom implements Cloneable {
    
    /**
     *
     * @param chromLength
     * @param bounds
     */
    public SchwefelChrom(int chromLength, double [][] bounds){
        
        super ("Schwefel", chromLength, bounds);
        
    }
    
    /**
     *
     * @param schwefelChrom
     */
    public SchwefelChrom (SchwefelChrom schwefelChrom){
        
        super ("Schwefel", schwefelChrom.getChromLength(), schwefelChrom.getBounds());
        super.setAlleles(schwefelChrom.getAlleles());
        
    }
    
    /**
     *
     */
    public SchwefelChrom (){
        
        super ("Schwefel", 10, new double [][] {{-500, 500}}); 
        
    }
    
    @Override
    public SchwefelChrom clone()throws CloneNotSupportedException{
        
        return (SchwefelChrom)super.clone(); 
    }
    
    /**
     *For evaluating the fitness of this particular chromosome
     */
    @Override
    public void evalFitness (){
        
        setFitness(0);
        
        final double V = 418.9829;
            
            double sum = 0.0;
            
            for (int i = 0; i < getChromLength(); i++){
                
                sum += getAlleles()[i]*Math.sin(Math.sqrt(Math.abs(getAlleles()[i])));
                
            }
             setFitness(getChromLength() * V - sum);
            
    }
    
}
