
package gaforfunctoptinherit;

import java.util.Random;
import java.util.Arrays;

/**
 *
 * @author Ummar Shehu
 */
public abstract class FunctChrom extends Chromosome {
    
    //an array reresenting the alleles
    private double [] alleles;
    
    //the variablerepesenting the randomly generated allele
    private double randAllele;
    
    //the variable representing the bounds of a coordinate of a vector
    private final double [][] bounds;
    
    //represents the function name as a string
    private final String functName;
            
    /**
     *
     * @param functName
     * @param chromLength
     * @param bounds
     */
    public FunctChrom (String functName, int chromLength, double [][] bounds) {
        
        super.setChromLength(chromLength);
        
        alleles = new double[chromLength];
        this.bounds = bounds;
        this.functName = functName;
        
    }
    
    @Override
    public Chromosome clone()throws CloneNotSupportedException{ 
        
        FunctChrom cloneFunChrom = (FunctChrom) super.clone();
        
        cloneFunChrom.setAlleles(alleles.clone());
        
        return cloneFunChrom;  
    }
    
    /**
     *
     * @param alleles
     */
    public void setAlleles(double [] alleles){
        
        this.alleles = alleles;
        
    }
    
    /**
     *
     * @param chromLoc
     * @param newRandom
     * @param lowerBound
     * @param upperBound
     */
    @Override
    public void genRandAllele(int chromLoc, Random newRandom, double lowerBound, 
            double upperBound){
        
        randAllele = 0;
        
        /*Each cordinate of  the vector has a lower and upper 
        bound, l and u respectively. These bounds can be the same for each coordinate 
        (i. e the value of l is same for each cordinateor and similarly the value of u 
        is same for each coordinate)or otherwise. 
        The following if statement checks if the function has the same boundries 
        for each coordinate.*/
       if (getBounds().length == 1){
          randAllele = getBounds()[0][0] + newRandom.nextDouble()*
                  (getBounds()[0][1] - getBounds()[0][0]);
       } 
       
       /*If otherwise, the coordinates have different boundaries, i.e 
       the pairs of values or intervals [l,u] are different for each cordinate.*/        
       else {
           randAllele = getBounds()[chromLoc][0] + newRandom.nextDouble()*
                   (getBounds()[chromLoc][1] - 
                        getBounds()[chromLoc][0]);
       }
       
    }
    
    @Override
    public void genRndAlleles (Random newRandom){
        
        for (int counter = 0; counter < getChromLength(); counter++){
            
            genRandAllele(counter, newRandom, 0, 0);
            
            getAlleles()[counter] = getRndAllele();
                   
            }
           
    }
    
    /**
     * 
     * @param index
     * @param allele 
     */
   @Override
   public void setAllele(int index, Object allele) {
       alleles[index] = (double) allele;
   }
    
   /**
    * 
    * @param locus
    * @return 
    */
   @Override
   public Object getAllele(int locus){
       
       return alleles[locus];
       
   }
   
    /**
     *
     * @return
     */
    public double [] getAlleles() {
       return alleles;
   }
   
    /**
     *
     * @param index
     * @param allele
     */
    public void setAllele(int index, double allele) {
       alleles[index] = allele;
   }
   
    /**
     *
     * @return
     */
    @Override
   public Double getRndAllele(){
       return randAllele;
   } 
   
    /**
     *
     * @return
     */
    public  double [][] getBounds(){
        return bounds;
    }
    
    /**
     *
     * @return
     */
    public  String getFunctType (){
        
        return functName;
    }
    
    @Override
    public String toString(){
        
        return String.format(
               "Function type: %s%nBounds:%s%nChromosome lenght:%s%nChromosome vaues:"
                       + "%s%nChromosome fitness:%n%s%n",
                /*Chromosome fitness:%s%nChromosome vaues:%n%s%n*/
                       getFunctType(), getBoundsEntries(),getChromLength(), 
                       Arrays.toString(getAlleles()),/*getStrChromVals(),*/
                               getFitness());
    }
    
    /**
     *This method returns the function type, function bounds, 
     *and function chromosome length, without the chromosome real entries (alleles)
     * @return
     */
    @Override
    public String getStrChrom(){
        
        return String.format(
               "Function type: %s%nBounds:%s%nChromosome Lenght:%s%n",
                /*Chromosome fitness:%s%nChromosome vaues:%n%s%n*/
                       getFunctType(), getBoundsEntries(), getChromLength());
    }
    
    /**
     *For obtaining the boundary value(s) of a function
     * @return
     */
    public String getBoundsEntries(){
        String returnStr = "";
        
        if (getBounds().length == 1){
           
            returnStr = getBounds()[0][0] + "," + " " + getBounds()[0][1];
           
       }
       
       else {
           
           for (int counter = 0; counter < getChromLength(); counter++){
                
                returnStr += getBounds()[counter][0] + "," + " " + getBounds()[counter][1] + "; ";
           
            }
        }
        
        return returnStr;
        
    }
    
}
