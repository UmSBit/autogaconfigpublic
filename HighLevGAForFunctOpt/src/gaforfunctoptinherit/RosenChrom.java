
package gaforfunctoptinherit;

/**
 *
 * @author Ummar Shehu 
 */
public class RosenChrom extends /*SameBndChrom*/ FunctChrom implements Cloneable {
    
    /**
     *
     * @param chromLength
     * @param bounds
     */
    public RosenChrom(int chromLength, double [][] bounds){
        
        super ("Rosenbrock", chromLength, bounds);
        
    }
    
    /**
     *
     * @param rosenChrom
     */
    public RosenChrom (RosenChrom rosenChrom){
        
        super ("Rosenbrock", rosenChrom.getChromLength(), rosenChrom.getBounds());
        super.setAlleles(rosenChrom.getAlleles());
        
    }
    
    /**
     *
     */
    public RosenChrom (){
        
        super ("Rosenbrock", 2, new double [][] {{-2.048, 2.048}});
       
    }
    
    @Override
    public RosenChrom clone()throws CloneNotSupportedException{
        
        RosenChrom cloneChrom = (RosenChrom)super.clone();
        return cloneChrom;  
        
    }
    
    /**
     *For evaluating the fitness of this particular chromosome
     */
    @Override
    public void evalFitness (){
        
        setFitness(0);
        
        for (int i = 0; i < getChromLength() - 1; i++){
                
                setFitness(getFitness() + 100 * Math.pow (getAlleles()[i + 1] - 
                        (Math.pow(getAlleles()[i], 2.0)), 2.0 ) + 
                        Math.pow( (1 - getAlleles()[i]), 2.0 )); 

        }
        
    }
   
}
