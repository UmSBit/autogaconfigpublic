
package gaforfunctoptinherit;

/**
 *
 * @author Ummar Shehu
 */

public class AddFunct2Chrom extends FunctChrom implements Cloneable {
    
    /**
     *
     * @param chromLength
     * @param bounds
     */
    public AddFunct2Chrom(int chromLength, double [][] bounds){
        
        super ("AddFunct2", chromLength, bounds);
        
    }
    
    /**
     *
     * @param addFunct2Chrom
     */
    public AddFunct2Chrom (AddFunct2Chrom addFunct2Chrom){
        
        super ("AddFunct2", addFunct2Chrom.getChromLength(), addFunct2Chrom.getBounds());
        super.setAlleles(addFunct2Chrom.getAlleles());
        
    }
    
    /**
     *
     */
    public AddFunct2Chrom (){
        super ("AddFunct2", 10, new double [][] {{-10, 10}});
    }
    
    @Override
    public AddFunct2Chrom clone()throws CloneNotSupportedException{
        
        return (AddFunct2Chrom)super.clone(); 
        
    }
    
    /**
     *For evaluating the fitness of this particular chromosome
     */
    @Override
    public void evalFitness (){
        
        setFitness(0);
        double sum, product;
        sum = 0;
        product = 1;
        
        for (int i = 0; i < getChromLength(); i++){
            sum += Math.abs(getAlleles()[i]);
            product *= Math.abs(getAlleles()[i]);
        }
        
        setFitness(sum + product);
        
    }
    
}
