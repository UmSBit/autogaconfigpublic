
package gaforfunctoptinherit;

/**
 *
 * @author Ummar Shehu 
 */
public class SphereChrom extends FunctChrom implements Cloneable {
    
    /**
     *
     * @param chromLength
     * @param bounds
     */
    public SphereChrom(int chromLength, double [][] bounds){
        
        super ("Sphere", chromLength, bounds);
        
    }
    
    /**
     *
     * @param sphereChrom
     */
    public SphereChrom (SphereChrom sphereChrom){
        
        super ("Sphere", sphereChrom.getChromLength(), sphereChrom.getBounds());
        super.setAlleles(sphereChrom.getAlleles().clone());
        
    }
    
    /**
     *
     */
    public SphereChrom (){
        
        super ("Sphere", 3, new double [][]{{-5.12, 5.12}}); 
       
    }
    
    @Override
    public SphereChrom clone()throws CloneNotSupportedException{
        
        return (SphereChrom)super.clone();  
        
    }
    
    /**
     *For evaluating the fitness of this particular chromosome
     */
    @Override
    public void evalFitness (){
        
        setFitness(0);
        
        for (int i = 0; i < getChromLength(); i++){
            
            setFitness(getFitness() + getAlleles()[i]*getAlleles()[i]);
            
        }
        
    }
   
}
