
package gaforfunctoptinherit;

import java.util.Arrays;

/**
 *
 * @author Ummar Shehu
 */
public class GA {
    
    private int noOfGenerations;
    
    private Chromosome bestRunChrom;
    private double bestRunFit;
    private final GAOperators newGAOp;
    
    private double percntageCrx;
    
    private double percntageMut;
    
    /**
     *
     * @param newGAOp
     */
    public GA(GAOperators newGAOp){
        
        this.newGAOp = newGAOp;
        
    }
    
    /**
     *
     * @return
     */
    public int getNoOfGenerations (){
        
        return noOfGenerations;
        
    }
   
    /**
     *For running the GA
     */
    public void run (){
        
        newGAOp.getInitialPop ().genInitPop(newGAOp.getRandGen());
         
         newGAOp.copyInitPopToCrntPop();
         
         newGAOp.getCurrentPop().calcPopFit();
         
         try {
         
            bestRunChrom = newGAOp.getCurrentPop ().getBestChrom().clone();
                 
            bestRunFit = bestRunChrom.getFitness();
            
            noOfGenerations++;
            
            while (noOfGenerations < newGAOp.getMaxGens()){
                 
                newGAOp.genNewPop (noOfGenerations);
                
                newGAOp.copyNewPopToCrntPop();
                
                if (newGAOp.getCurrentPop ().getBestChrom().getFitness() < bestRunFit) {
                
                    bestRunChrom = newGAOp.getCurrentPop ().getBestChrom().clone();
                
                bestRunFit = bestRunChrom.getFitness();
                    
                }
                
                    noOfGenerations++;
            
                }
            
         }catch(CloneNotSupportedException c){}
         
    }
    
    /**
     *
     * @return Best Run Chromosome
     */
    public Chromosome getBstRnChrom(){
        return bestRunChrom;
    }
    
    /**
     *
     * @return
     */
    public double getBestRnFt(){
        return bestRunFit;
    }
    
    /**
     *
     * @return
     */
    public GAOperators getGAOp(){
        return newGAOp;
    }
    
     @Override
     public String toString(){
         return String.format("%n%nGenetic Operators: %n%s" + "%n%nBest Run Chromosome: %s", 
                 newGAOp, bestRunChrom);
     }
     
    /**
     *
     * @return
     */
    public String getStrGA(){
          
         String StrChrmVals = "";
         
         if (newGAOp.getCurrentPop().getPopChromType() instanceof FunctChrom){
             
            FunctChrom Chrom;
            Chrom = (FunctChrom) bestRunChrom;
            StrChrmVals = Arrays.toString(Chrom.getAlleles());
            
         }
         
         return String.format("%n%nGenetic Operators: %n%s" + "%n%nBest Run Chromosome values: %s" +  
                 "%nBest Run Chromosome fitness: %s", newGAOp.getStrGAOp(), StrChrmVals,
                 bestRunChrom.getFitness());
         
     }
     
}
