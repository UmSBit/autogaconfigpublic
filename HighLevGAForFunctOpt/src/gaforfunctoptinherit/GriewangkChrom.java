
package gaforfunctoptinherit;

/**
 *
 * @author Ummar Shehu 
 */
public class GriewangkChrom extends /*SameBndChrom*/ FunctChrom implements Cloneable {
    
    /**
     *
     * @param chromLength
     * @param bounds
     */
    
    public GriewangkChrom(int chromLength, double [][] bounds){
        
        super ("Griewangk", chromLength, bounds);
       
    }
    
    /**
     *
     * @param griewangkChrom
     */
    public GriewangkChrom (GriewangkChrom griewangkChrom){
        
        super ("Griewangk", griewangkChrom.getChromLength(), griewangkChrom.getBounds());
        super.setAlleles(griewangkChrom.getAlleles());
        
    }
    
    /**
     *
     */
    public GriewangkChrom (){
        
        super ("Griewangk", 10, new double [][] {{-600, 600}}); 
        
    }
    
    @Override
    public GriewangkChrom clone()throws CloneNotSupportedException{
        
        return (GriewangkChrom)super.clone();  
    }
    
    /**
     *
     */
   @Override
    public void evalFitness (){
        
        setFitness(0);
        
        double sum = 0.0;
            double product = 1.0;
            for (int i = 0; i < getChromLength(); i++){
                sum += getAlleles()[i]*getAlleles()[i]/4000;
                product *= Math.cos(getAlleles()[i]/Math.sqrt(i+1));

            }
            setFitness(1 + sum - product);
            
    }
    
}
