The software system is about a GA (High Level GA, HLGA) used to automatically 
design another GA (Low Level GA, LLGA) applied on function optimization. Both 
the HLGA and the LLGA follow the GA pseudo code presented in the write-up, and 
the details of both GAs (like population size, selection method, crossover rate, 
and so on) were also presented in the write-up.
The software can be logically divided into two parts. The first part is the 
manually designed GA (MDGA) whose performance was compared with automatically 
esigned GA. The second part is about automatically evolving the configuration 
of the LLGA by the HLGA.
To run any part of the system, you need to specify that "part" in the code.
To run the first part: comment all the "GAOpeators" instances except the 
following instance (with the indicated parameters and their values) given by 
the code snippet: 
"new GAOperators (/*chromType*/Functions.HYPER_HEURS_FUNS[k],
                        /*popSize*/500, /*seed*/RandomSeeds.SEEDS[j], 
                        /*selnMethType*/2, /*crosovType*/1, 
                        /*crossoverRate*/0.8, 
                        /*mutatnType*/3, /*mutationRate*/1, 
                        /*nonUnifMutationRate*/0.06, 
                        /*shapeParameter*//*0.05*/13.0, /*tournamentSize*/20, 
                        /*maxNoOfGens*/1000);"
in the "multiFunMultiRun" method of the "GAMultiRun" class in the 
"gaforfunctoptinherit" package.
Moreover, since the LLGA is run several times you need to indicate all seeds in 
the for loop for the seeds. Therefore, this for loop,  also, in the 
"multiFunMultiRun" method of the "GAMultiRun" class, should be in the form: 
"for (int j = 0; j < RandomSeeds.SEEDS.length; j++)"
Running the LLGA is not time consuming, so it can be run for all functions to 
replicate the results (except for Quartic function, F3, which uses random seed 
dependent on the time for which the function is being evaluated) in the 
write-up. Hence to run the LLGA for all functions the for loop specifying the 
function to be use should indicate all functions by having the for loop in the 
"multiFunMultiRun" method of the "GAMultiRun" class in the following form:
"for (int k = 0; k < Functions.HYPER_HEURS_FUNS.length ; k++)"
Finally, after the above modifications, the LLGA can now be run by running the 
main class of the" gaforfunctoptinherit package"; the results will be 
reproduced as in the write-up except for Quartic function because of the reason 
explained above.

To evolve the LLGA configuration by the HLGA, the following modification should 
be implemented:
The HLGA is run once; therefore,  one seed is used in the for loop indicating 
the seeds. That is the for loop , in the "multiFunMultiRun" method of the 
"GAMultiRun" class , should be in this form:
"for (int j = 0; j < 1; j++)"  
Next, since evolving the LLGA configuration requires a lot of computation, the 
configuration for one function is evolved at a time. To specify the associated 
function for the HLGA; the index of the  desired function in the functions 
array must be used. This array is of length 10; index 0 represents F1, index 1 
represents F2, and so on. To run the system with F1, use 0 and 1 in the code 
snippet, 
"for(int k = 0; k < 1; k++);" 
in the "multiFunMultiRun" method of the "GAMultiRun" class to run the system 
with F2,  use "1 and 2" in the code snippet, and so on.
To specify the GA configuration system, comment all instances of the 
GAOperators class except the instance for evolving the configuration. The 
instance use for evolving the configuration is:
"new GAOperators (/*chromosome type*/gAConfigChrom, /*population Size*/30, 
                    /*Random object*/gAConfigChrom.getRandom(),
                    /*selection method type*/2, /*crosover type*/1, 
                    /*crossover rate*/0.8, /*mutation type*/2, 
                    /*mutation rate*/0.05, /*tournament size*/2, 
                    /*number of generations*/50);"
To replicate the evolved configuration for any function, simply use that 
function and the GAOperators instance in the code as explained above; and the 
parameters and their values as given in the code snippet.
