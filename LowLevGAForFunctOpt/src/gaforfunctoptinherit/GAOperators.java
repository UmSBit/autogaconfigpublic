package gaforfunctoptinherit;
/**
 *
 * @author Ummar Shehu
 */

import java.util.Random;


public class GAOperators implements Cloneable{
    
    private final Population initialPop;//The initial population
    private Population currentPop;//The current population
    private final Population newPopulation;//The new population
    
    //Number of generations
    private final int maxNoOfGens;
    private Random newRandom;
    private final double mutationRate;
    private final double crossoverRate;
    private final int tournamentSize;
    
    //The non-uniform mutation rate
    private final double nonUnifMutationRate;
    //The shape parameter of the non-uniform mutation
    private final double shapeParameter;
    
    //An array of the pair of selected chromosome
    private final Chromosome [] selectedChroms;
    
    //An array of the pair of selected chromosome consisting of FunctChrom chromosome type
    private final FunctChrom [] selFunctChroms;
    
    /*The selnMethType can take three possible integral values (0, 1, 2).
    0 represente random selecton method, 
    1 represents fitness proportionate selecton method,
    and 2 represents tournament selecton method.
    */
    private final int selnMethType;
    
    /*The crosovType can take four possible integral values (0, 1, 2, 3).
    0 represents no crossover, 1 represents one-point crossover, 2 represent two-point crossover,
    and 3 uniform crossover.
    */
    private final int crosovType;
    
    /*The mutatnType can take four possible integral values (0, 1, 2, 3).
    0 represents no mutation, 1 represents mutation at a random locus, 
    2 represents mutation at a each locus,and 3 represents non-uniform mutation.
    */
    private final int mutatnType;
    
    //Selection method type in words
    private String strSelnType;
    
    //Crossover type in words    
    private String strCrxType;
    
     //Mutation type in words
    private String strMutType;
     
    /**
     *
     * @param chromType
     * @param popSize
     * @param seed
     * @param selnMethType
     * @param crosovType
     * @param crossoverRate
     * @param mutatnType
     * @param mutationRate
     * @param nonUnifMutationRate
     * @param shapeParameter
     * @param tournamentSize
     * @param maxNoOfGens
     */
    public GAOperators (Chromosome chromType, int popSize, long seed, int selnMethType, 
            int crosovType, double crossoverRate, int mutatnType, double mutationRate, 
            double nonUnifMutationRate, double shapeParameter,int tournamentSize, 
            int maxNoOfGens){
       
        initialPop = new Population (chromType, popSize);
        currentPop = new Population (initialPop);
        newPopulation = new Population (initialPop);
        
        this.selnMethType = selnMethType;
        
        this.crosovType = crosovType;
        
        this.crossoverRate = crossoverRate;
        
        this.mutatnType = mutatnType;
        
        this.mutationRate = mutationRate;
        
        this.tournamentSize = tournamentSize;
        this.maxNoOfGens = maxNoOfGens;
        
        this.nonUnifMutationRate = nonUnifMutationRate;
        this.shapeParameter = shapeParameter;
        
        newRandom = new Random(seed);
        
        selectedChroms = new Chromosome [2];
        
        selFunctChroms = new FunctChrom [2];
        
        setStrGAOpsTypes();
        
    }
    
    /**
     *
     * @param chromType
     * @param popSize
     * @param newRandom
     * @param selnMethType
     * @param crosovType
     * @param crossoverRate
     * @param mutatnType
     * @param mutationRate
     * @param nonUnifMutationRate
     * @param shapeParameter
     * @param tournamentSize
     * @param maxNoOfGens
     */
    public GAOperators (Chromosome chromType, int popSize, Random newRandom, int selnMethType, 
            int crosovType, double crossoverRate, int mutatnType, double mutationRate, 
            double nonUnifMutationRate, double shapeParameter,int tournamentSize, 
            int maxNoOfGens){
       
        initialPop = new Population (chromType, popSize);
        currentPop = new Population (initialPop);
        newPopulation = new Population (initialPop);
        
        this.selnMethType = selnMethType;
        
        this.crosovType = crosovType;
        
        this.crossoverRate = crossoverRate;
        
        this.mutatnType = mutatnType;
        
        this.mutationRate = mutationRate;
        
        this.tournamentSize = tournamentSize;
        this.maxNoOfGens = maxNoOfGens;
        
        this.nonUnifMutationRate = nonUnifMutationRate;
        this.shapeParameter = shapeParameter;
        
        this.newRandom = newRandom;
        
        selectedChroms = new Chromosome [2];
        
        selFunctChroms = new FunctChrom [2];
        
        setStrGAOpsTypes();
        
    }
    
    /**
     *
     * @param chromType
     * @param seed
     */
    public GAOperators (Chromosome chromType, long seed){
         
         initialPop = new Population (chromType, 500);
         currentPop = new Population (initialPop);
         newPopulation = new Population (initialPop);
         mutationRate = 0.05;
         
         selnMethType = 2;
         
         mutatnType = 3;
         
         crosovType = 2;
        
         crossoverRate = 0.8;
         tournamentSize = 15;
         maxNoOfGens = 1000;
        
         nonUnifMutationRate = 0.05;
         shapeParameter = 0.06;
        
         newRandom = new Random(seed);
        
        selectedChroms = new Chromosome [2];
        
        selFunctChroms = new FunctChrom [2];
        
        setStrGAOpsTypes();
    }
    
    /**
     *
     * @param chromType
     * @param popSize
     * @param newRandom
     * @param selnMethType
     * @param crosovType
     * @param crossoverRate
     * @param mutatnType
     * @param mutationRate
     * @param tournamentSize
     * @param maxNoOfGens
     */
    public GAOperators (Chromosome chromType, int popSize, Random newRandom, int selnMethType, 
            int crosovType, double crossoverRate, /**/int mutatnType, double mutationRate,
            int tournamentSize, int maxNoOfGens){
       
         initialPop = new Population (chromType, popSize);
         currentPop = new Population (initialPop);
         newPopulation = new Population (initialPop);
        
         this.newRandom = newRandom;
         
         this.selnMethType = selnMethType;
        
         this.crosovType = crosovType;
        
         this.mutatnType = mutatnType;
        
         this.mutationRate = mutationRate;
        
         this.crossoverRate = crossoverRate;
         this.tournamentSize = tournamentSize;
         this.maxNoOfGens = maxNoOfGens;
        
         nonUnifMutationRate = 0.04;
         shapeParameter = 0.05;
        
         selectedChroms = new Chromosome [2];
        
         selFunctChroms = new FunctChrom [2];
         
         setStrGAOpsTypes();
        
    }
    /**
     *
     * @param newGAOperators
     */
    protected GAOperators(GAOperators newGAOperators){
        
        Population tempInitialPop = null;
        Population tempCurrentPop = null;
        Population tempNewPopulation = null;
                    
        try{
            
            tempInitialPop = (Population) newGAOperators.initialPop.clone();
            tempCurrentPop = (Population) newGAOperators.currentPop.clone();
            tempNewPopulation = (Population) newGAOperators.newPopulation.clone();
            
            }catch(CloneNotSupportedException c){}
            
        initialPop = tempInitialPop;
        currentPop = tempCurrentPop;
        newPopulation = tempNewPopulation;
        
        maxNoOfGens = newGAOperators.maxNoOfGens;
        newRandom = newGAOperators.newRandom;
        mutationRate = newGAOperators.mutationRate;
        crossoverRate = newGAOperators.crossoverRate;
        tournamentSize = newGAOperators.tournamentSize;
            
        nonUnifMutationRate = newGAOperators.nonUnifMutationRate;
        shapeParameter = newGAOperators.shapeParameter;
            
        selectedChroms = newGAOperators.selectedChroms;
    
        selFunctChroms = newGAOperators.selFunctChroms;
            
        selnMethType = newGAOperators.selnMethType;
                    
        crosovType = newGAOperators.crosovType;
    
        mutatnType = newGAOperators.mutatnType;
    
        strSelnType = newGAOperators.strSelnType;
        
        strCrxType = newGAOperators.strCrxType;
        
        strMutType = newGAOperators.strMutType;        
         
    }
    
    @Override
    public GAOperators clone()throws CloneNotSupportedException{
        return  new GAOperators(this);
    }
    
    /**
     *Interpreting the GA parameters in numbers to words
     */
    public void setStrGAOpsTypes(){
        
        if (selnMethType == 0){
            
            strSelnType = "Random Selection";
            
        }
        
        if (selnMethType == 1){
            
            strSelnType = "Fitness Proportionate Selection";
            
        }
        
        if (selnMethType == 2){
            
            strSelnType = "Tournament Selection";
            
        }
        
        if (crosovType == 0){
            
            strCrxType = "No Crossover";
            
        }
        
        if (crosovType == 1){
            
            strCrxType = "One-Point Crossover";
            
        }
        
        if (crosovType == 2){
            
            strCrxType = "Two-Point Crossover";
            
        }
        
        if (crosovType == 3){
            
            strCrxType = "Uniform Crossover";
            
        }
        
        if (mutatnType == 0){
            
            strMutType = "No Mutation";
            
        }
        
        if (mutatnType == 1){
            
            strMutType = "Mutation at Randomly Selected Locus";
            
        }
        
        if (mutatnType == 2){
            
            strMutType = "Mutation at Each Locus";
            
        }
        
        if (mutatnType == 3){
            
            strMutType = "Non-Uniform Mutation";
            
        }
        
    }
    
    /**
     *
     * @param seed
     */
    public void setSeed  (long seed){
        
        newRandom = new Random(seed); 
        
    }
    
    /**
     *
     * @return
     */
    public Random getRandGen (){
        return newRandom;
    }
    
    /**
     *
     * @return
     */
    public Population getInitialPop (){
        return initialPop;
    }
    
    /**
     *
     * @return
     */
    public Population getCurrentPop (){
        return currentPop;
    }
    
    /**
     *
     * @return
     */
    public Population getNewPop (){
        return newPopulation;
    }
    
    /**
     *
     * @return
     */
    public int getMaxGens (){
        return maxNoOfGens;
    }
    
    /**
     *
     * @return
     */
    public Chromosome [] getSelChroms(){
        
        return selectedChroms;
        
    }
    
    /**
     *
     * @return
     */
    public double getCrosovRate() {
        return crossoverRate;
    }
    
    /**
     *
     * @return
     */
    public double getMutRate() {
        
        return mutationRate;
        
    }
    
    /**
     *
     * @return
     */
    public double getNUMutRate() {
        
        return nonUnifMutationRate;
        
    }
    
    /**
     *
     * @return
     */
    public int getSelMethTyp (){
        
        return selnMethType;
        
    }
    
    /**
     *
     * @return
     */
    public int getCrxType (){
        
        return crosovType;
        
    }
    
    /**
     *
     * @return
     */
    public int getMutType (){
        
        return mutatnType;
        
    }
    
    /**
     *
     * @return
     */
    public int getTournSize (){
        
        return tournamentSize;
        
    }
    
    /**
     *For making the initial population to become the current population
     */
    public void copyInitPopToCrntPop(){
        
        currentPop = new Population(initialPop);
       
    }
    
    /**
     *For making the new population to become the  current population
     */
    public void copyNewPopToCrntPop(){
        
        currentPop = new Population(newPopulation);
        
        currentPop.setBestChrom(newPopulation.getBestChrom());
        currentPop.setWorstChrom(newPopulation.getWorstChrom());
        
    }
    
    /**
     *Method for tournament selection
     * @return selected chromosome from tournament election
     */
    public Chromosome tournSelection (){
        
        Chromosome [] tournamentChroms = new Chromosome [tournamentSize];
        int randIndex, selChromIndInPop;
        randIndex = newRandom.nextInt(currentPop.getPopSize());
        selChromIndInPop = randIndex;
        Chromosome bestChrom = null;
       
        try{
            
            bestChrom = currentPop.getPopIndividuals()[randIndex].clone();
            
            for (int i = 1; i < tournamentSize; i++){
            
                randIndex = newRandom.nextInt(currentPop.getPopSize());
                tournamentChroms[i] = currentPop.getPopIndividuals()[randIndex].clone();
                
                if (tournamentChroms[i].getFitness() < bestChrom.getFitness()){
                    
                    selChromIndInPop = randIndex;
                    bestChrom = tournamentChroms[i].clone();
                    
                }
                
            }
               
        }catch(CloneNotSupportedException c){}
        
        return bestChrom;
        
    }
    
    /**
     *The selection method, the particular selection type is specified by the 
      *particular of the GAOperator class
     */
    public void selectChroms (){
        
      if (selnMethType == 0){
          
          selectWthRndSel();
      }   
      
      else if (selnMethType == 1){
          
          selectWithFitProp();
      }   
      
      else if (selnMethType == 2){
          
          selectWithTourn();
      }   
      
    }
    
    //

    /**
     *Method for random selection method
     * @return selected chromosome from random selection
     */
    public Chromosome randSelect(){
        
        Chromosome rndlySelChrom = null;
        int randIndex = newRandom.nextInt(currentPop.getPopSize());
        
        try{
            
            rndlySelChrom = getCurrentPop().getPopIndividuals()[randIndex].clone();
        
        }catch(CloneNotSupportedException c){}
        
        return rndlySelChrom; 
        
    }
    
    /**
     * @return selected chromosome from fitness proportionate selection
     * Method for fitness proportionate selection 
     */
    private Chromosome fitPropSel (){
        
        double sumOfFitness = 0.0;
        double randomValue;

        double sum = 0.0;
        Chromosome chromSelected = null;
        
        for (int i = 0; i < currentPop.getPopSize (); i++){
            
            sumOfFitness += currentPop.getPopIndividuals()[i].getFitness();
                     
        }
        
        randomValue = newRandom.nextDouble()*sumOfFitness;
        
        for (int i = 0; i < currentPop.getPopSize (); i++){
            
            sum += currentPop.getPopIndividuals()[i].getFitness();
            
            if ( sum >= randomValue /*&& count == 0*/) {
               
              try{
            
                chromSelected = getCurrentPop().getPopIndividuals()[i].clone();
        
              }catch(CloneNotSupportedException c){}
              
              break;
                
            }
           
        }
        
        return chromSelected;
   } 
   
    /**
     *For selecting the pair of parent chromosome with fitness proportionate selection
     * @return selected chromosomes from fitness proportionate selection
     */
   public Chromosome [] selectWithFitProp (){
        
        try{
            for (int i = 0; i < selectedChroms.length; i++){
            
                selectedChroms[i] = fitPropSel ().clone();
                
            }
        
        }catch(CloneNotSupportedException c){}
        
        return selectedChroms;
        
    }
    
    /**
     *For selecting the pair of parent chromosome with random selection
     * @return selected chromosomes selected  with random selection
     */
    public Chromosome [] selectWthRndSel (){
        
        try{
            for (int i = 0; i < selectedChroms.length; i++){
            
                selectedChroms[i] = randSelect ().clone();
                
            }
        
        }catch(CloneNotSupportedException c){}
        
        return selectedChroms;
        
    }
    
    /**
     *For selecting the pair of parent chromosome with tournament selection
     * @return selected chromosomes from tournament selection
     */
    public Chromosome [] selectWithTourn (){
        
        try{
            for (int i = 0; i < selectedChroms.length; i++){
            
                selectedChroms[i] = tournSelection ().clone();
                
            }
        
        }catch(CloneNotSupportedException c){}
        
        return selectedChroms;     
    }
    
    /**
     *For swapping the alleles at the specified locus during crossover
     * @param chromIndex
     */
    public void swap(int chromIndex){
        
        double tempVal;
         tempVal = (double)selectedChroms[0].getAllele(chromIndex);
         selectedChroms[0].setAllele(chromIndex, selectedChroms[1].getAllele(chromIndex));
         selectedChroms[1].setAllele(chromIndex, tempVal);
        
    }
    
    /**
     *The crossover method, the particular crossover type is specified by the 
     *particular of the GAOperator class
     */
    public void crossoverChroms () {
       
      if (crosovType == 0){
               
         //No crossover
                
      }
      
      else if (crosovType == 1) {
          
          //For variable length chromosome one-point crossover
          if (selectedChroms[0].getChromLength() != 
                    selectedChroms[1].getChromLength()){
             
              varLen1PtCrx();
              
          }
          
          else {
              
                crossover1pt ();
                
          }
            
      }
        
      else if (crosovType == 2) {
          
          //For variable length chromosome 2-pt crossover
          if (selectedChroms[0].getChromLength() != 
                    selectedChroms[1].getChromLength()){
                  
                 varLen2ptCrx (); 
                  
          }
          
          else {
              
                crossover2pt ();
                
          }
      }
        
      else if (crosovType == 3) {
          
          if (selectedChroms[0].getChromLength() != 
                    selectedChroms[1].getChromLength()){
                  
                 varLenUnifCrx (); 
                  
          }
          
          else {
              
                uniflyCrosovChroms ();
                
          }
                
      }
      
        
    }
    
    /**
     *One point crossover
     * @return
     */
    public Chromosome [] crossover1pt () {
        
        double randomValue = newRandom.nextDouble();
        
        if ( crossoverRate >= randomValue){ 
         
            int randomLocus = newRandom.nextInt(selectedChroms[0].getChromLength ());
            
            for (int j = /*0*/(randomLocus + 1); j < selectedChroms[0].getChromLength (); j++){
             
                swap(j);
                
               }
            
        }
        
        return selectedChroms; 
    }
    
    /**
     *Two point crossover
     * @return chromosomes after crossover
     */
    public Chromosome [] crossover2pt () {
        
        int higherValLoc, lowerValLoc; 
        
        double randomValue = newRandom.nextDouble();
        
        if ( crossoverRate >= randomValue){   
            
            int randomLocus1 = newRandom.nextInt(selectedChroms[0].getChromLength ());
            int randomLocus2 = newRandom.nextInt(selectedChroms[0].getChromLength ());
            if ( randomLocus2 >= randomLocus1){
                higherValLoc = randomLocus2 ;
                lowerValLoc = randomLocus1 ;
            }
            else {
                higherValLoc = randomLocus1 ;
                lowerValLoc = randomLocus2 ;               
            }
            
            //So if the lowerValLoc = higherValLoc, the following for loop will not be executed
            for (int j = (lowerValLoc + 1); j <= higherValLoc; j++){
                
               swap(j);
                
            }
            
        }
        
        return selectedChroms;   
    }
    
    /**
     * For uniform crossover
     * @return chromosomes after uniform crossover
     */
    public Chromosome [] uniflyCrosovChroms () {
        
        for (int j = 0; j < selectedChroms[0].getChromLength (); j++){
            
            double randomValue = newRandom.nextDouble();
        
            if (0.5 >= randomValue){ 
            
                swap(j);
                
            }
            
        }
        
        return selectedChroms;
    }
    
    

    /**
     *One-pt Crossover for variable length chromosome
     * @return chromosomes after variable length crossover
     */
    public Chromosome [] varLen1PtCrx () {
        
        double randomValue = newRandom.nextDouble();
        
        if ( crossoverRate >= randomValue){ 
            
            int smallerChromLen;
            
            if (selectedChroms[0].getChromLength() < 
                    selectedChroms[1].getChromLength()){
                smallerChromLen = selectedChroms[0].getChromLength();
            }
            
            else {
                smallerChromLen = selectedChroms[1].getChromLength();
            }
            
            int randomLocus = newRandom.nextInt(smallerChromLen);
            
            for (int j = (randomLocus + 1); j < smallerChromLen; j++){
            
               swap(j);
                
                }
            }
            
        return selectedChroms;
    }
    
    
    /**
     *Two point crossover for variable length chromosome
     * @return chromosomes after crossover
     */
    public Chromosome [] varLen2ptCrx () {
        
        double randomValue = newRandom.nextDouble();
        
        if ( crossoverRate >= randomValue){ 
            
            /*Since the two parent chromosome may be of different lengths,
              the random crossover point is selected from the smaller lenght chromosome.
            */
            int smallerChromLen;
            
            if (selectedChroms[0].getChromLength() < 
                    selectedChroms[1].getChromLength()){
                smallerChromLen = selectedChroms[0].getChromLength();
            }
            
            else {
                smallerChromLen = selectedChroms[1].getChromLength();
            }
            
        int higherValLoc, lowerValLoc; 
        
        int randomLocus1 = newRandom.nextInt(smallerChromLen);
        int randomLocus2 = newRandom.nextInt(smallerChromLen);
          
            if ( randomLocus2 >= randomLocus1){
                higherValLoc = randomLocus2 ;
                lowerValLoc = randomLocus1 ;
            }
            
            else {
                higherValLoc = randomLocus1 ;
                lowerValLoc = randomLocus2 ;               
            }
        
            for (int j = (lowerValLoc + 1); j <= higherValLoc; j++){
                
               swap(j);
               
            }
            
        }
        
        return selectedChroms;   
    }
    
    /**
     *Uniform crossover for variable length chromosome
     * @return
     */
    public Chromosome [] varLenUnifCrx () {
        
        /*Since the two parent chromosome may be of different lengths,
              the random crossover point is selected from the smaller lenght chromosome.
            */
            int smallerChromLen;
            
            if (selectedChroms[0].getChromLength() < 
                    selectedChroms[1].getChromLength()){
                smallerChromLen = selectedChroms[0].getChromLength();
            }
            
            else {
                smallerChromLen = selectedChroms[1].getChromLength();
            }
            
        for (int j = 0; j < smallerChromLen; j++){
            double randomValue = newRandom.nextDouble();
        
            if (0.5 >= randomValue){ 
            
                swap(j);
                
            }
            
            else {
                
            }
                
            
        }
        
        return selectedChroms;   
    }
    
    /**
     *For mutation, the particular mutation type is specified by the 
      particular instance of the GAOperator class
     * @param noOfGens
     */
    public void mutateChroms(int noOfGens){
       
        if (mutatnType == 0){
            //No mutation
        }
        
        if (mutatnType == 1){
            mutateAtRandomLoc();
        }
        
        else if (mutatnType == 2){
            mutateAtEachLoc();
        }
        
        else if (mutatnType == 3){
            applyNUMutation(noOfGens);
        }
        
    }
    
    /**
     *For non-uniform mutation applied on the pair of selected parents
     * @param noOfGens
     * @return
     */
    public Chromosome [] applyNUMutation(int noOfGens){
       
        double diffBtwUBandAlleles;
        double diffBtwAllelesandLB;

        double exponent; 
        
        exponent = Math.pow((1 - (double) noOfGens/maxNoOfGens),shapeParameter);
        
        for (int i = 0; i < selectedChroms.length; i++){
            
            for (int j = 0; j < selectedChroms[i].getChromLength (); j++){
                
                double randomValue = newRandom.nextDouble();
                
                if ( nonUnifMutationRate >= randomValue){
                    
                  selFunctChroms[i] = (FunctChrom) selectedChroms[i];
                  
                    if (selFunctChroms[i].getBounds().length == 1){
                        
                        diffBtwUBandAlleles = selFunctChroms[i]
                                .getBounds()[0][1] - 
                                selFunctChroms[i].getAlleles()[j];
                        
                        diffBtwAllelesandLB = selFunctChroms[i].getAlleles()[j] -
                                selFunctChroms[i].getBounds()[0][0];
                        
                    }
        
                    else {
                        
                        diffBtwUBandAlleles = selFunctChroms[i].getBounds()[j][1] - 
                                selFunctChroms[i].getAlleles()[j];
                        
                        diffBtwAllelesandLB = selFunctChroms[i].getAlleles()[j] -
                              selFunctChroms[i].getBounds()[j][0];
                        
                    }
                     
                    if (newRandom.nextInt(2) == 0){
                        
                        selFunctChroms[i].setAllele(j, selFunctChroms[i].getAlleles()[j] + 
                                diffBtwUBandAlleles * (1 - Math.pow(randomValue, exponent)));
                        
                    }
                    else {
                        
                        selFunctChroms[i].setAllele(j, selFunctChroms[i].getAlleles()[j] - 
                                diffBtwAllelesandLB * (1 - (Math.pow(randomValue, exponent))));
                        
                    }
                    
                }   
            }
            
        }
        
        return selectedChroms;
    }
    
    /**
     *For mutation at each locus
     * @return
     */
    public Chromosome [] mutateAtEachLoc () {
        
        for (Chromosome selectedOffspng : selectedChroms) {
           
            for (int j = 0; j < selectedOffspng.getChromLength (); j++){
                
                double randomValue = newRandom.nextDouble();
                
                if ( mutationRate >= randomValue){
                    
                    selectedOffspng.genRandAllele(j, newRandom, 0, 0);
                    selectedOffspng.setAllele(j, selectedOffspng.getRndAllele());
                    
                    
                }   
            }  
            
        }
        
        return selectedChroms; 
    }
    
    /**
     *For mutation at a single randomly chosen locus
     * @return
     */
    public Chromosome [] mutateAtRandomLoc () {
         
        for (Chromosome selectedOffspng : selectedChroms) {
            
            int randomLocus = newRandom.nextInt(selectedOffspng.getChromLength ());
            double randomValue = newRandom.nextDouble();
            
                if ( mutationRate >= randomValue){
                    
                   selectedOffspng.genRandAllele(randomLocus, newRandom, 0, 0);
                    selectedOffspng.setAllele(randomLocus, selectedOffspng.getRndAllele());
                    
                }
                
        }
        
        return selectedChroms;
    }
    
    /**
     *For Method implementing elitism
     * @param currentPop
     * @param newPop
     */
    public void elitism (Population currentPop, Population newPop){
        
        try{
        if (currentPop.getBestChrom().getFitness() < newPop.getBestChrom().getFitness()){
            
            newPop.getPopIndividuals()[newPop.getWorstFtIndex()] = 
                    currentPop.getPopIndividuals()[currentPop.getBestFtIndex()].clone();
            
        }
        
        else
        {
            
            int randIndInPop;
            randIndInPop = newRandom.nextInt(initialPop.getPopSize());
            
            newPop.getPopIndividuals()[randIndInPop] = currentPop.getBestChrom().clone();
            
            
        }
        
        }catch(CloneNotSupportedException c){}
        
    }
    
    /**
     *For adding the two "children" to the new population
     * @param Insertion Index of th population
     */
    public void addSelChromsToNewPop(int popInsertInd){
        try{
        
            for (Chromosome chromSel : selectedChroms) {
                newPopulation.getPopIndividuals()[popInsertInd] = chromSel.clone();
                popInsertInd++;
            }
        
        }catch(CloneNotSupportedException c){}
    }   
    
    /**
     *For adding each child at a specified index of the new population
     * @param selChrom
     * @param popInsertInd
     */
    public void addSelChromToNewPop(Chromosome selChrom, int popInsertInd){
        try{
            
            newPopulation.getPopIndividuals()[popInsertInd] = selChrom.clone();
            
        }catch(CloneNotSupportedException c){}
    }   
    
    /**
     *For generating new population from the current population
     * @param noOfGens
     */
    public void genNewPop (int noOfGens){
        
        for (int i = 0; i < currentPop.getPopSize();) {
           
           selectChroms();
           
           crossoverChroms ();
           
           mutateChroms(noOfGens);
           
           addSelChromToNewPop(selectedChroms[0], i);
           addSelChromToNewPop(selectedChroms[1], (i + 1));
           i += 2;
           
        }
        
        newPopulation.calcPopFit();
        
        elitism (currentPop, newPopulation);
        newPopulation.findBestChrom();
        newPopulation.findWorstChrom();
        
    }
    
    @Override
    public String toString(){
       
        return String.format("%s" + "Mutation rate: %s%n" + 
                "%nNon Uniform Mutation rate: %s%n" + "%nCrossover rate: %s%nTournamentSize: " + 
                "%s%nNo. of Generations: %s%n%n", 
                getCurrentPop(), mutationRate, nonUnifMutationRate, crossoverRate, 
                tournamentSize, maxNoOfGens);
       
        
    }

    /**
     *For string representation of the GAOpeator object without displaying the population individuals
     * @return
     */
    public String getStrGAOp(){
        
        return String.format("%s" + "%nSelection Method: %s%n" + 
                "%nCrossover Type: %s%n" + 
                "%nMutation Type: %s%n" + 
                "%nCrossover rate: %s%n" +
                "%nMutation rate: %s%n" + 
                "%nNon Uniform Mutation rate: %s%n" + 
                "%nShape Parameter: %s%n" + 
                "%nTournamentSize: %s%n" + 
                "%nNo. of Generations: %s%n", 
                
                getCurrentPop().getStrPop(), strSelnType, 
                strCrxType, 
                strMutType, 
                crossoverRate,
                mutationRate, 
                nonUnifMutationRate, 
                shapeParameter, 
                tournamentSize, 
                maxNoOfGens);
        
    }
    
}