
package gaforfunctoptinherit;

import java.util.Random;

/**
 *
 * @author Ummar Shehu 
 */
public abstract class Chromosome {
   
    //The chromosome length
    private int chromLength ;
    
    //The chromsome finess
    private double chromFitness;
    
    /**
     *Every concrete subclass implements this method for its fitness evaluation
     */
    public abstract void evalFitness ();
    
    /**
     *For returning randomly generated allele
     * @return
     */
    public abstract Object getRndAllele();
    
    /**
     *
     * @param chromLength
     */
    public void setChromLength(int chromLength){
        this.chromLength = chromLength;
    }  
    
    /**
     *
     * @param chromFitness
     */
    public void setFitness(double chromFitness){
        this.chromFitness = chromFitness;
    }  
    
    /**
     * 
     * @param index
     * @param allele 
     */
    public abstract void setAllele(int index, Object allele);
    
    /**
     * 
     * @param locus
     * @return 
     */
    public abstract Object getAllele(int locus);
    
    /**
     *
     * @return
     */
    public double getFitness() {
        
        return chromFitness;
    } 
    
    /**
     *This method is for generating a (single) random allele at a specific locus 
     *between the specified lower and upper bounds
     * @param chromLoc
     * @param newRandom
     * @param lowerBound
     * @param upperBound
     */

    public abstract void genRandAllele(int chromLoc, Random newRandom, double lowerBound, 
            double upperBound);
    
    /**
     *This method is for generating (all) random alleles of a chromosome 
     * @param newRandom
     */
    public abstract void genRndAlleles (Random newRandom);
    
    @Override
    public String toString(){
        
        return String.format(
               "Chromosome lenght:%s%n", getChromLength());
    }
    
    /**
     *This implementation is only included because a 
     *subclass oveeriding this method can be called from a variable of this class. 
     *The method is not made abstract because some subclasses do not require the method's implementation.
     *The method is for returning details of a chromosome without the alleles.
     * @return
     */
    public String getStrChrom(){
        
        return "";
    }
    
    /**
     *
     * @return
     */
    public int getChromLength(){
        return chromLength;
    }
    
    @Override
    public Chromosome clone()throws CloneNotSupportedException{ 
        
        return (Chromosome) super.clone(); 
    }
    
}
