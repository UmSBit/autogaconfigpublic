
package gaforfunctoptinherit;

import java.util.Random;
/**
 *
 * @author Ummar Shehu
 */

public class AddFunct1Chrom extends FunctChrom implements Cloneable {
    
    /**
     *
     * @param chromLength
     * @param bounds
     */
    public AddFunct1Chrom(int chromLength, double [][] bounds){
        
        super ("AddFunct1", chromLength, bounds);
        
    }
    
    /**
     *
     * @param addFunct1Chrom
     */
    public AddFunct1Chrom (AddFunct1Chrom addFunct1Chrom){
        
        super ("AddFunct1", addFunct1Chrom.getChromLength(), addFunct1Chrom.getBounds());
        super.setAlleles(addFunct1Chrom.getAlleles());
        
    }
    
    /**
     *
     */
    public AddFunct1Chrom (){
        super ("AddFunct1", 10, new double [][] {{-10, 10}});
    }
    
    @Override
    public AddFunct1Chrom clone()throws CloneNotSupportedException{
        
        return (AddFunct1Chrom)super.clone(); 
        
    }
    
    /**
     *For evaluating the fitness of this particular chromosome
     */
    @Override
    public void evalFitness (){
        
        setFitness(0);
        
        for (int i = 0; i < getChromLength(); i++){
            
            setFitness(getFitness() + (i + 1) * getAlleles()[i]*getAlleles()[i]);
            
        }
    }
    
}
