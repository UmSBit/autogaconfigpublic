
package gaforfunctoptinherit;


import java.util.Random;

/**
 *
 * @author Ummar Shehu 
 */
public class QuarticChrom extends /*SameBndChrom*/ FunctChrom implements Cloneable {
    
    /**
     *
     * @param chromLength
     * @param bounds
     */
    public QuarticChrom(int chromLength, double [][] bounds){
        
        super ("Quartic", chromLength, bounds);
        
    }
    
    /**
     *
     * @param quarticChrom
     */
    public QuarticChrom (QuarticChrom quarticChrom){
        
        super ("Quartic", quarticChrom.getChromLength(), quarticChrom.getBounds());
        super.setAlleles(quarticChrom.getAlleles());
       
    }
    
    /**
     *
     */
    public QuarticChrom (){
        
        super ("Quartic", 30, new double [][] {{-1.28, 1.28}}); 
        
    }
    
    @Override
    public QuarticChrom clone()throws CloneNotSupportedException{
        
        return (QuarticChrom)super.clone(); 
        
    }
    
    /**
     *For evaluating the fitness of this particular chromosome
     */
   @Override
    public void evalFitness (){
        
        setFitness(0);
        
        long seed;
        seed = System.currentTimeMillis();
        
        final Random randGen = new Random (seed);        
                
        double random;
        for (int i = 0; i < getChromLength(); i++){
             
            random = randGen.nextDouble();
            
            setFitness(getFitness() + (i+1) * Math.pow(getAlleles()[i], 4.0) + random);
               
        }
        
    }
   
}
