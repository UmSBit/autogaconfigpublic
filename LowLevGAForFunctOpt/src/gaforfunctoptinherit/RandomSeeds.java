
package gaforfunctoptinherit;

/**
 *
 * @author Ummar Shehu
 */
public class RandomSeeds {

    /**
     *
     */
    public static final long [] SEEDS = 
    {
        677703, 757817, 786559, 767107, 226997, 874782, 327064, 730451, 634259, 743400,	
        593008, 809847, 828440, 884568, 460020, 373194, 84970, 527832,	986363, 131616,	
        41300,	136261,	185344,	486180,	838934,	834205,	565323,	394960,	79445,	724678
            
    };

    /**
     *
     * @return
     */
    public long [] getRandomSeeds(){
        return SEEDS;
    }
    
}
