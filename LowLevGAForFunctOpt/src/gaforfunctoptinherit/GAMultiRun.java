 
package gaforfunctoptinherit;

/**
 *
 * @author Ummar Shehu
 */


public class GAMultiRun {
    
    private final double [] fitsOfBsRns = new double [RandomSeeds.SEEDS.length];
    private double avgFitOfRuns;// = 0;
    private double stdDevOfRuns;// = 0;
    private double bstFtForAllRns = 0;
    private final Statistics newStat = new Statistics ();
    
    /**
     *For multiple runs of GA on one or more functions
     */
    public void multiFunMultiRun (){
        
        GA bestGA = null;
        
        //try (BufferedWriter writer = 
                
           // new BufferedWriter (new FileWriter ("C:\\Users\\USER\\Desktop\\Result.csv"))){
         
        for (int k = 0; k < Functions.HYPER_HEURS_FUNS.length; k++){
            
            for (int j = 0; j < RandomSeeds.SEEDS.length; j++){
                
                GAOperators newGAOperators;
                GA newGA;
                        
                newGAOperators = 
                        
                /*The (low-level heuristic (GA))GAOperatos constructor with the 
                tuned parameter for functions commonly used in hyper-heuristics*/
                new GAOperators (/*chromType*/Functions.HYPER_HEURS_FUNS[k],
                        /*popSize*/500, /*seed*/RandomSeeds.SEEDS[j], 
                        /*selnMethType*/2, /*crosovType*/1, /*crossoverRate*/0.8, 
                        /*mutatnType*/3, /*mutationRate*/1, /*nonUnifMutationRate*/0.06, 
                        /*shapeParameter*//*0.05*/13.0, /*tournamentSize*/20, /*maxNoOfGens*/1000);
                    
         newGA = new GA(newGAOperators);
                        
         newGA.run();
                        
         fitsOfBsRns[j] = newGA.getBestRnFt();
                        
         if (j == 0){
                            
            bstFtForAllRns = newGA.getBestRnFt();
                            
            bestGA = newGA;
                           
            //System.out.println(bestGA.getGAOp().getStrGAOp() + "\n");
                            
         }
                        
         else {
               if (bstFtForAllRns > newGA.getBestRnFt()){
                   
                   bstFtForAllRns = newGA.getBestRnFt();
                                
                   bestGA = newGA;
                                
                }
             }
                    
        }
                
                avgFitOfRuns = newStat.calcAverage (fitsOfBsRns);
                stdDevOfRuns = newStat.calcStdDev(fitsOfBsRns);
                
                //System.out.println("Best Chromosome for all runs: \n" + bestGA.getBstRnChrom() + "\n");
                
             //if (bestGA.getBstRnChrom() instanceof FunctChrom){
                    System.out.println(((FunctChrom)bestGA.getBstRnChrom()).getFunctType() + ":");
                    System.out.println(String.format("Average Fitness: " + "%.2E", avgFitOfRuns));
                    //writer.write(String.format("" + "%.4E", avgFitOfRuns));
                    //writer.newLine();
                    
             //}
            
         }
           
        //}    
        //catch (IOException e) {
        //e.printStackTrace();
        //}
    }
    
}
