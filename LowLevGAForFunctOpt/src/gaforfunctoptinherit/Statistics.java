package gaforfunctoptinherit;

import java.util.Arrays;

/**
 *
 * @author Ummar Shehu
 */
public class Statistics {

    /**
     *
     * @param dataPoints
     * @return
     */
    public double calcAverage (double [] dataPoints){
        
        double sum;
        sum = 0;
        double average;
        for (int i = 0; i < dataPoints.length; i++){
            
            sum += dataPoints[i];
            
        }
        
        average = sum/dataPoints.length;
       
        return average;
    }
    
    /**
     *
     * @param dataPoints
     * @return
     */
    public double calcStdDev (double [] dataPoints){
        
        double sum;
        sum = 0.0;
        double average;
        double stdDev;
        average = calcAverage (dataPoints);
        
        for (int i = 0; i < dataPoints.length; i++){
            
            sum += (dataPoints[i] - average)*(dataPoints[i] - average);
            
        }
        
        stdDev = Math.sqrt(sum/(dataPoints.length - 1));
        return stdDev;
    }
    
}
