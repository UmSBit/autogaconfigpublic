
package gaforfunctoptinherit;

/**
 *
 * @author Ummar Shehu 
 */
public class AckleyChrom extends FunctChrom implements Cloneable {
    
    /**
     *
     * @param chromLength
     * @param bounds
     */
    public AckleyChrom(int chromLength, double [][] bounds){
        
        super ("Ackley", chromLength, bounds);
        
    }
    
    /**
     *
     * @param ackleyChrom
     */
    public AckleyChrom (AckleyChrom ackleyChrom){
       
        super ("Ackley", ackleyChrom.getChromLength(), ackleyChrom.getBounds());
        super.setAlleles(ackleyChrom.getAlleles());
       
    }
    
    /**
     *
     */
    public AckleyChrom (){
        
        super ("Ackley", 20, new double [][] {{-30, 30}}); 
        
    }
    
    @Override
    public AckleyChrom clone()throws CloneNotSupportedException{
        
        AckleyChrom cloneChrom = (AckleyChrom)super.clone();
        
        return cloneChrom;  
        
    }
    
    /**
     *For evaluating the fitness of this particular chromosome
     */
    @Override
    public void evalFitness (){
        
        setFitness(0);
        
        double sum1 = 0.0;
        double sum2 = 0.0;
        
        for (int i = 0; i < getChromLength(); i++){
            
            sum1 += getAlleles()[i]*getAlleles()[i];
            sum2 += Math.cos(2*Math.PI*getAlleles()[i]);
                
        }
        
             setFitness(20 + Math.E - 20*Math.pow(Math.E, -0.2*Math.sqrt(sum1/getChromLength()))
                    - Math.pow(Math.E, (sum2/getChromLength())));
        
        
    }
    
}
