
package gaforfunctoptinherit;

import java.util.Random;

/**
 *
 * @author Ummar Shehu 
 */
public class RastriginChrom extends FunctChrom implements Cloneable {
   
    /**
     *
     * @param chromLength
     * @param bounds
     */
    public RastriginChrom(int chromLength, double [][] bounds){
        
        super ("Rastrigin", chromLength, bounds);
       
    }
    
    /**
     *
     * @param rastriginChrom
     */
    public RastriginChrom (RastriginChrom rastriginChrom){
        
        super ("Rastrigin", rastriginChrom.getChromLength(), rastriginChrom.getBounds());
        super.setAlleles(rastriginChrom.getAlleles());
        
    }
    
    /**
     *
     */
    public RastriginChrom (){
        
        super ("Rastrigin", 20, new double [][] {{-5.12, 5.12}}); 
       
    }
    
    
    
    @Override
    public RastriginChrom clone()throws CloneNotSupportedException{
        
        return (RastriginChrom)super.clone(); 
        
    }
    
    /**
     *
     */
    @Override
    public void evalFitness (){
        
        setFitness(0);
        
        final int A = 10;
            
            double sum = 0.0;
            
            for (int i = 0; i < getChromLength(); i++){
            
            sum += (getAlleles()[i]*getAlleles()[i]) - A*Math.cos(2*Math.PI*getAlleles()[i]);
            
            }
        
            setFitness(A * getChromLength() + sum);
            
    }
    
}
