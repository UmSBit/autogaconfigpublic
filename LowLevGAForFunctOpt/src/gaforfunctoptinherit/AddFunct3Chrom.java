
package gaforfunctoptinherit;

import java.util.Random;

/**
 *
 * @author Ummar Shehu
 */

public class AddFunct3Chrom extends FunctChrom implements Cloneable {
    
    /**
     *
     * @param chromLength
     * @param bounds
     */
    public AddFunct3Chrom(int chromLength, double [][] bounds){
        
        super ("AddFunct3", chromLength, bounds);
        
    }
    
    /**
     *
     * @param addFunct3Chrom
     */
    public AddFunct3Chrom (AddFunct3Chrom addFunct3Chrom){
        
        super ("AddFunct3", addFunct3Chrom.getChromLength(), addFunct3Chrom.getBounds());
        super.setAlleles(addFunct3Chrom.getAlleles());
        
    }
    
    /**
     *
     */
    public AddFunct3Chrom (){
        
        super ("AddFunct3", 10, new double [][] {{-100, 100}});
    }
    
    @Override
    public AddFunct3Chrom clone()throws CloneNotSupportedException{
        
        return (AddFunct3Chrom)super.clone(); 
        
    }
    
    /**
     *For evaluating the fitness of this particular chromosome
     */
    @Override
    public void evalFitness (){
        
        setFitness(0);
        double sum, sqrOfSum;
        sum = 0;
        sqrOfSum = 0;
        for (int i = 0; i < getChromLength(); i++){
            
             sum += getAlleles()[i]; 
             sqrOfSum += Math.pow(sum, 2);
                
        }
        
        setFitness(sqrOfSum);
    }
    
}
